var canvas = document.getElementById("diamondCanvas");
var ctx = canvas.getContext("2d");

function drawDiamond() {
    drawleftTop();
    drawleftBottom();
    drawrightTop();
    drawrightBottom();
} 

function drawleftTop() {
    ctx.moveTo(150,0);
    ctx.lineTo(0,150);
    ctx.stroke();
}

function drawleftBottom() {
    ctx.moveTo(0,150);
    ctx.lineTo(150,300);
    ctx.stroke();
}

function drawrightTop() {
    ctx.moveTo(300,150);
    ctx.lineTo(150,300);
    ctx.stroke();
}

function drawrightBottom() {
    ctx.moveTo(150,0);
    ctx.lineTo(300,150);
    ctx.stroke();
}